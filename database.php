<?php
header('Access-Control-Allow-Origin: *');
$connect = mysqli_connect("localhost","smartgoalstraining_com_smartgoalgetters","no12ah","smartgoalstraining_com_smartgoalgetters");

if (isset($_GET['loadusers'])) {
	$RetrieveAll = "SELECT * FROM userdata";
	$RetrieveAllResult = mysqli_query($connect, $RetrieveAll);

	$SetToRows = array();
	while($r = mysqli_fetch_assoc($RetrieveAllResult)) {
		$SetToRows[] = $r;
	}
	echo json_encode(utf8ize($SetToRows));
}

if (isset($_GET['deleteuser'])) {
	$ID = $_GET['ID'];
	$Delete = "DELETE FROM userdata WHERE ID = ". $ID;
	$DeleteResult = mysqli_query($connect, $Delete);
}

if (isset($_GET['savedata'])) {
	$ID = $_GET['ID'];
	$Accepted = $_GET['Accepted'];
	$PreviousID = $_GET['PreviousID'];
	$NextID = $_GET['NextID'];
	$Name = $_GET['Name'];
	$ProfilePicture = $_GET['ProfilePicture'];
	$Location = $_GET['Location'];
	$Sports = $_GET['Sports'];
	$Occupation = $_GET['Occupation'];
	$ShortOccupation = $_GET['ShortOccupation'];
	$FieldOfExpertise = $_GET['FieldOfExpertise'];
	$AssociatedClub = $_GET['AssociatedClub'];
	$AssociatedClubImage = $_GET['AssociatedClubImage'];
	$Biography = $_GET['Biography'];
	$WhyDoITrain = $_GET['WhyDoITrain'];
	$UserFile = $_GET['UserFile'];
	$SocialMedia = $_GET['SocialMedia'];
	$FacebookLink = $_GET['FacebookLink'];
	$TwitterLink = $_GET['TwitterLink'];
	$InstagramLink = $_GET['InstagramLink'];
	$LinkedinLink = $_GET['LinkedinLink'];
	$WebsiteLink = $_GET['WebsiteLink'];
	$Email = $_GET['Email'];

	$SaveQuery = "UPDATE `userdata` SET `ID`= ". $ID .",`Accepted`= ". $Accepted .",`PreviousID`= ". $PreviousID .",`NextID`= ". $NextID .",`Name`= '". $Name ."',`ProfilePicture`= '". $ProfilePicture ."',`Location`= '". $Location ."',`Sports`= '". $Sports ."',`Occupation`= '". $Occupation ."',`ShortOccupation`= '". $ShortOccupation ."',`FieldOfExpertise`= '". $FieldOfExpertise ."',`AssociatedClub`= '". $AssociatedClub ."',`AssociatedClubImage`= '". $AssociatedClubImage ."',`Biography`= '". $Biography ."',`WhyDoITrain`= '". $WhyDoITrain ."',`UserFile`= '". $UserFile ."',`SocialMedia`= '". $SocialMedia ."',`FacebookLink`= '". $FacebookLink ."',`TwitterLink`= '". $TwitterLink ."',`InstagramLink`= '". $InstagramLink ."',`LinkedinLink`= '". $LinkedinLink ."',`WebsiteLink`= '". $WebsiteLink ."',`Email`= '". $Email ."' WHERE ID = ". $ID;
	$SaveResult = mysqli_query($connect, $SaveQuery);
}

if (isset($_GET['newgetter'])) {
	$Accepted = $_GET['Accepted'];
	$PreviousID = $_GET['PreviousID'];
	$NextID = $_GET['NextID'];
	$Name = $_GET['Name'];
	$ProfilePicture = $_GET['ProfilePicture'];
	$Location = $_GET['Location'];
	$Sports = $_GET['Sports'];
	$Occupation = $_GET['Occupation'];
	$ShortOccupation = $_GET['ShortOccupation'];
	$FieldOfExpertise = $_GET['FieldOfExpertise'];
	$AssociatedClub = $_GET['AssociatedClub'];
	$AssociatedClubImage = $_GET['AssociatedClubImage'];
	$Biography = $_GET['Biography'];
	$WhyDoITrain = $_GET['WhyDoITrain'];
	$UserFile = $_GET['UserFile'];
	$SocialMedia = $_GET['SocialMedia'];
	$FacebookLink = $_GET['FacebookLink'];
	$TwitterLink = $_GET['TwitterLink'];
	$InstagramLink = $_GET['InstagramLink'];
	$LinkedinLink = $_GET['LinkedinLink'];
	$WebsiteLink = $_GET['WebsiteLink'];
	$Email = $_GET['Email'];

	$NewGetterQuery = "INSERT INTO `userdata`(`Accepted`, `PreviousID`, `NextID`, `Name`, `ProfilePicture`, `Location`, `Sports`, `Occupation`, `ShortOccupation`, `FieldOfExpertise`, `AssociatedClub`, `AssociatedClubImage`, `Biography`, `WhyDoITrain`, `UserFile`, `SocialMedia`, `FacebookLink`, `TwitterLink`, `InstagramLink`, `LinkedinLink`, `WebsiteLink`, `Email`) VALUES ('". $Accepted ."','". $PreviousID ."','". $NextID ."','". $Name ."','". $ProfilePicture ."','". $Location ."','". $Sports ."','". $Occupation ."','". $ShortOccupation ."','". $FieldOfExpertise ."','". $AssociatedClub ."','". $AssociatedClubImage ."','". $Biography ."','". $WhyDoITrain ."','". $UserFile ."','". $SocialMedia ."','". $FacebookLink ."','". $TwitterLink ."','". $InstagramLink ."','". $LinkedinLink ."','". $WebsiteLink ."','". $Email ."')";
	$NewGetterResult = mysqli_query($connect, $NewGetterQuery);
	echo $NewGetterQuery;
}

function utf8ize($d) {
	if (is_array($d)) {
		foreach ($d as $k => $v) {
			$d[$k] = utf8ize($v);
		}
	} else if (is_string ($d)) {
		return utf8_encode($d);
	}
	return $d;
}

mysqli_close($connect);
?>