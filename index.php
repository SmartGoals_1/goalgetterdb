<!DOCTYPE html>
<html>
<head>
	<title>GoalGetters Edit</title>
	<meta charset="UTF-8">
	<meta name="keywords" content="Smartgoals smart goals getters"/>
	<meta name="description" content="Become a SmartGoal Getter now, and achieve the impossible!"/>
	<meta name="author" content="Noah V. Schijndel"/>
	<meta name="copyright" content=""/>
	<meta http-equiv="ScreenOrientation" content="autoRotate:disabled">
	<meta name="title" content="SmartGoalGetters">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link href="CSS\Design.css" type="text/css" rel="stylesheet" id="NormalStyle"/>
	<link href="CSS\normalize.css" type="text/css" rel="stylesheet" />
	<link href="CSS\override.css" type="text/css" rel="stylesheet" />
	<link href="CSS\animate.css" type="text/css" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css?family=Mukta+Mahee" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Biryani" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="JS/Main.js"></script>
	<script src="CSS/WOW-master/dist/wow.min.js"></script>
</head>
<body>
	<div class="table-title">
		<h3>GoalGetter Database</h3>
	</div>
	<div class="loader"></div>

	<div class="button-wrapper">
		<div class="InfoButton" onclick="openHelp('HandleidingGoalGetters.pdf');">General Info</div>
		<div class="AddGoalGetter" onclick="AddGoalgetter();">Add a Goalgetter</div>
	</div>
	<div class="table-wrapper">
		<table class="table-fill">
			<thead>
				<tr>
					<th>Edit / Delete</th>
					<th>ID</th>
					<th>Accepted</th>
					<th>PreviousID</th>
					<th>NextID</th>
					<th>Name</th>
					<th>ProfilePicture</th>
					<th>Location</th>
					<th>Sports</th>
					<th>Occupation</th>
					<th>ShortOccupation</th>
					<th>FieldOfExpertise</th>
					<th>AssociatedClub</th>
					<th>AssociatedClubImage</th>
					<th>Biography</th>
					<th>WhyDoITrain</th>
					<th>UserFile</th>
					<th>SocialMedia</th>
					<th>FacebookLink</th>
					<th>TwitterLink</th>
					<th>InstagramLink</th>
					<th>LinkedinLink</th>
					<th>WebsiteLink</th>
					<th>Email</th>
				</tr>
			</thead>
			<tbody class="table-hover" id="SetTable"></tbody>
		</table>
	</div>

	<div class="AcceptDeleteDiv">
		<div class="AcceptDeleteCloseButton" onclick="ReturnOverlay();">X</div>
		<span id="AcceptDeleteHeader">Are you sure you want to delete this Goalgetter? <br /> (This action can not be reverted!)</span><br />
		<div class="DeleteConfirmButton">Delete this Goalgetter</div>
	</div>

	<div id="Overlay" onclick="ReturnOverlay();"></div>
</body>
</html>